insert into companies(id, name, parent_id)
values (1, 'C1', null),
       (2, 'C2', null),
       (3, 'C3', 1),
       (4, 'C4', 1),
       (5, 'C5', 2),
       (6, 'C6', 2);

insert into structural_units(id, name, company_id, parent_id)
values (1, 'U1', 1, null),
       (2, 'U2', 1, null),
       (3, 'U3', null, 1),
       (4, 'U4', null, 1),
       (5, 'U5', null, 3),
       (6, 'U6', null, 3),
       (7, 'U7', 3, null),
       (8, 'U8', 3, null),
       (9, 'U9', null, 7),
       (10, 'U10', null, 8),
       (11, 'U11', null, 9),
       (12, 'U12', null, 10);
