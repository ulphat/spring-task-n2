package net.bakhishoff.task.springtaskn2.controller;

import net.bakhishoff.task.springtaskn2.model.entity.Company;
import net.bakhishoff.task.springtaskn2.model.entity.StructuralUnit;
import net.bakhishoff.task.springtaskn2.model.mapper.CompanyMapper;
import net.bakhishoff.task.springtaskn2.repository.CompanyRepository;
import net.bakhishoff.task.springtaskn2.service.CompanyService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Ulphat
 */
@WebMvcTest({CompanyController.class, CompanyService.class, CompanyMapper.class})
class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CompanyRepository repository;


    @Test
    void createAndExpectOk() throws Exception {
        Company c1 = new Company();
        c1.setId(1);
        c1.setName("c1");
        when(repository.save(any(Company.class))).thenReturn(c1);

        mockMvc.perform(post("/companies")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"c1\"}"))
               .andExpect(status().isCreated());
    }

    @Test
    void createAndExpect422() throws Exception {
        mockMvc.perform(post("/companies")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"\"}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void list() throws Exception {
        Company c1 = new Company();
        c1.setName("c1");
        c1.setId(1);
        Company c2 = new Company();
        c2.setName("c2");
        c2.setId(2);
        c2.setParent(c1);
        Company c3 = new Company();
        c3.setName("c3");
        c3.setId(3);
        c3.setParent(c1);

        List<Company> list = List.of(c1, c2, c3);
        when(repository.findAll()).thenReturn(list);
        mockMvc.perform(get("/companies"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.*", hasSize(3)));
    }

    @Test
    void deleteAndExpectOk() throws Exception {
        Company c1 = new Company();
        c1.setId(1);
        when(repository.findById(1L)).thenReturn(Optional.of(c1));
        mockMvc.perform(delete("/companies/1")).andExpect(status().isNoContent());
    }

    @Test
    void deleteAndExpect422() throws Exception {
        Company c1 = new Company();
        c1.setId(1);
        c1.getSubCompanies().add(new Company());
        when(repository.findById(1L)).thenReturn(Optional.of(c1));
        mockMvc.perform(delete("/companies/1")).andExpect(status().isUnprocessableEntity());
    }

    @Test
    void deleteAndExpect422_() throws Exception {
        Company c1 = new Company();
        c1.setId(1);
        c1.getUnits().add(new StructuralUnit());
        when(repository.findById(1L)).thenReturn(Optional.of(c1));
        mockMvc.perform(delete("/companies/1")).andExpect(status().isUnprocessableEntity());
    }

    @Test
    void deleteAndExpect404() throws Exception {
        when(repository.findById(1L)).thenReturn(Optional.empty());
        mockMvc.perform(delete("/companies/1")).andExpect(status().isNotFound());
    }
}
