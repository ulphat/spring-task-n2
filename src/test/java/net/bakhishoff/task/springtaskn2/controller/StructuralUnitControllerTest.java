package net.bakhishoff.task.springtaskn2.controller;

import net.bakhishoff.task.springtaskn2.model.entity.Company;
import net.bakhishoff.task.springtaskn2.model.entity.StructuralUnit;
import net.bakhishoff.task.springtaskn2.model.mapper.CompanyMapper;
import net.bakhishoff.task.springtaskn2.model.mapper.StructuralUnitMapper;
import net.bakhishoff.task.springtaskn2.repository.CompanyRepository;
import net.bakhishoff.task.springtaskn2.repository.StructuralUnitRepository;
import net.bakhishoff.task.springtaskn2.service.StructuralUnitService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Ulphat
 */
@WebMvcTest({StructuralUnitController.class,
             StructuralUnitService.class,
             StructuralUnitMapper.class,
             CompanyMapper.class})
class StructuralUnitControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CompanyRepository companyRepository;
    @MockBean
    private StructuralUnitRepository repository;

    @Test
    void createAndExpectOk() throws Exception {
        when(repository.save(any(StructuralUnit.class))).thenReturn(new StructuralUnit());
        when(companyRepository.save(any(Company.class))).thenReturn(new Company());
        when(repository.existsById(any(Long.class))).thenReturn(true);

        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"structuralUnitId\": 1}"))
               .andExpect(status().isCreated());
    }

    @Test
    void createWithCompanyIdAndStructuralUnitIdAndExpect422() throws Exception {
        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"companyId\": 1,\"structuralUnitId\": 1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void createWithDuplicateNameAndExpect422() throws Exception {
        when(repository.existsByName(any(String.class))).thenReturn(true);

        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"companyId\": 1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void createWithEmptyNameAndExpect422() throws Exception {
        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"\",\"companyId\": 1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void createWithNonPositiveCompanyIdAndExpect422() throws Exception {
        when(companyRepository.existsById(-1L)).thenReturn(false);
        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"u1\",\"companyId\": -1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void createWithNonPositiveParentIdAndExpect422() throws Exception {
        when(repository.existsById(-1L)).thenReturn(false);
        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"u1\",\"structuralUnitId\": -1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateAndExpectOk() throws Exception {
        when(repository.findById(1L)).thenReturn(Optional.of(new StructuralUnit(1, new StructuralUnit(3))));
        when(repository.findById(2L)).thenReturn(Optional.of(new StructuralUnit()));
        when(repository.save(any(StructuralUnit.class))).thenReturn(new StructuralUnit());
        when(repository.existsById(any(Long.class))).thenReturn(true);

        mockMvc.perform(put("/units/1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"structuralUnitId\": 2}"))
               .andExpect(status().isOk());
    }

    @Test
    void updateWithParentAndExpect422() throws Exception {
        when(repository.findById(1L)).thenReturn(Optional.of(new StructuralUnit(1)));

        mockMvc.perform(put("/units/1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"structuralUnitId\": 2}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithCompanyAndExpect422() throws Exception {
        when(repository.findById(1L)).thenReturn(Optional.of(new StructuralUnit(1)));

        mockMvc.perform(put("/units/1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"companyId\": 2}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithCircledParentAndExpect422() throws Exception {
        StructuralUnit u1 = new StructuralUnit(1);
        StructuralUnit u2 = new StructuralUnit(2, u1);
        StructuralUnit u3 = new StructuralUnit(3, u2);
        when(repository.findById(1L)).thenReturn(Optional.of(u1));
        when(repository.findById(3L)).thenReturn(Optional.of(u3));

        mockMvc.perform(put("/units/1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"structuralUnitId\": 3}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithDuplicateNameAndExpect422() throws Exception {
        StructuralUnit u1 = new StructuralUnit(1, "u1", null);
        StructuralUnit u2 = new StructuralUnit(2, "string1", u1);
        StructuralUnit u3 = new StructuralUnit(3, "u3", null);
        when(repository.existsByNameAndIdNot("u1", 2L)).thenReturn(true);
        when(repository.findById(2L)).thenReturn(Optional.of(u2));
        when(repository.findById(3L)).thenReturn(Optional.of(u3));

        mockMvc.perform(put("/units/2")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"u1\",\"structuralUnitId\": 3}"))
               .andDo(print())
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithCompanyIdAndStructuralUnitIdAndExpect422() throws Exception {
        mockMvc.perform(put("/units/2")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"u1\",\"companyId\": 1,\"structuralUnitId\": 3}"))
               .andDo(print())
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithEmptyNameAndExpect422() throws Exception {
        mockMvc.perform(put("/units/2")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"\",\"structuralUnitId\": 3}"))
               .andDo(print())
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithNonPositiveCompanyIdAndExpect422() throws Exception {
        when(companyRepository.existsById(-1L)).thenReturn(false);
        mockMvc.perform(put("/units/1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"u1\",\"companyId\": -1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithNonPositiveParentIdAndExpect422() throws Exception {
        when(repository.existsById(-1L)).thenReturn(false);
        mockMvc.perform(put("/units/2")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"u1\",\"structuralUnitId\": -1}"))
               .andExpect(status().isUnprocessableEntity());
    }
    @Test
    void list() throws Exception {
        StructuralUnit u1 = new StructuralUnit(1);
        StructuralUnit u2 = new StructuralUnit(2, u1);
        StructuralUnit u3 = new StructuralUnit(3, u2);
        Company c1 = new Company();
        c1.setUnits(List.of(u1, u2));
        Company c2 = new Company();
        c2.setUnits(List.of(u3));
        c1.setSubCompanies(List.of(c2));

        when(companyRepository.findAllByParentIsNull()).thenReturn(List.of(c1));
        mockMvc.perform(get("/units"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.*", hasSize(1)));
    }

    @Test
    void deleteAndExpectOk() throws Exception {
        StructuralUnit u1 = new StructuralUnit(1);
        when(repository.findById(1L)).thenReturn(Optional.of(u1));
        mockMvc.perform(delete("/units/1")).andExpect(status().isNoContent());
    }

    @Test
    void deleteAndExpect422() throws Exception {
        StructuralUnit u1 = new StructuralUnit(1);
        StructuralUnit u2 = new StructuralUnit(1, u1);
        u1.setSubUnits(List.of(u2));
        when(repository.findById(1L)).thenReturn(Optional.of(u1));
        mockMvc.perform(delete("/units/1")).andExpect(status().isUnprocessableEntity());
    }

    @Test
    void deleteAndExpect404() throws Exception {
        when(repository.findById(1L)).thenReturn(Optional.empty());
        mockMvc.perform(delete("/units/1")).andExpect(status().isNotFound());
    }
}
