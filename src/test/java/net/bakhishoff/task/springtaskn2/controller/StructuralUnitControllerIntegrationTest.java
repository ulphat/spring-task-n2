package net.bakhishoff.task.springtaskn2.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Ulphat
 */
@Testcontainers
@SpringBootTest
@AutoConfigureMockMvc
class StructuralUnitControllerIntegrationTest {

    @Container
    public static final MySQLContainer MYSQL_CONTAINER = new MySQLContainer<>(DockerImageName.parse("mysql:8.0.19"));

    @DynamicPropertySource
    public static void overrideContainerProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("spring.datasource.url", MYSQL_CONTAINER::getJdbcUrl);
        dynamicPropertyRegistry.add("spring.datasource.username", MYSQL_CONTAINER::getUsername);
        dynamicPropertyRegistry.add("spring.datasource.password", MYSQL_CONTAINER::getPassword);
    }

    @Autowired
    private MockMvc mockMvc;

    @Test
    void createAndExpectOk() throws Exception {
        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"structuralUnitId\": 1}"))
               .andExpect(status().isCreated());
    }

    @Test
    void createWithCompanyIdAndStructuralUnitIdAndExpect422() throws Exception {
        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"companyId\": 1,\"structuralUnitId\": 1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void createWithDuplicateNameAndExpect422() throws Exception {
        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"U1\",\"companyId\": 1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void createWithEmptyNameAndExpect422() throws Exception {
        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"\",\"companyId\": 1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void createWithNonPositiveCompanyIdAndExpect422() throws Exception {
        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"u1\",\"companyId\": -1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void createWithNonPositiveParentIdAndExpect422() throws Exception {
        mockMvc.perform(post("/units")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"u1\",\"structuralUnitId\": -1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateAndExpectOk() throws Exception {
        mockMvc.perform(put("/units/3")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"UU1\",\"structuralUnitId\": 4}"))
               .andExpect(status().isOk());
    }

    @Test
    void updateWithParentAndExpect422() throws Exception {
        mockMvc.perform(put("/units/1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"structuralUnitId\": 2}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithCompanyAndExpect422() throws Exception {
        mockMvc.perform(put("/units/3")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"companyId\": 2}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithCircledParentAndExpect422() throws Exception {
        mockMvc.perform(put("/units/1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"string1\",\"structuralUnitId\": 6}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithDuplicateNameAndExpect422() throws Exception {
        mockMvc.perform(put("/units/2")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"U1\",\"structuralUnitId\": 3}"))
               .andDo(print())
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithCompanyIdAndStructuralUnitIdAndExpect422() throws Exception {
        mockMvc.perform(put("/units/2")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"u1\",\"companyId\": 1,\"structuralUnitId\": 3}"))
               .andDo(print())
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithEmptyNameAndExpect422() throws Exception {
        mockMvc.perform(put("/units/2")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"\",\"structuralUnitId\": 3}"))
               .andDo(print())
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithNonPositiveCompanyIdAndExpect422() throws Exception {
        mockMvc.perform(put("/units/1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"u1\",\"companyId\": -1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void updateWithNonPositiveParentIdAndExpect422() throws Exception {
        mockMvc.perform(put("/units/2")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\": \"u1\",\"structuralUnitId\": -1}"))
               .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void list() throws Exception {
        mockMvc.perform(get("/units"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.*", hasSize(2)));
    }

    @Test
    void deleteAndExpectOk() throws Exception {
        mockMvc.perform(delete("/units/6")).andExpect(status().isNoContent());
    }

    @Test
    void deleteAndExpect422() throws Exception {
        mockMvc.perform(delete("/units/1")).andExpect(status().isUnprocessableEntity());
    }

    @Test
    void deleteAndExpect404() throws Exception {
        mockMvc.perform(delete("/units/100")).andExpect(status().isNotFound());
    }
}
