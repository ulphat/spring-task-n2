create table companies
(
    id        bigint auto_increment
        primary key,
    name      varchar(255) null,
    parent_id bigint       null,
    constraint UK_name
        unique (name),
    constraint FK_company_parent_id
        foreign key (parent_id) references companies (id)
);
create table structural_units
(
    id         bigint auto_increment
        primary key,
    name       varchar(255) null,
    company_id bigint       null,
    parent_id  bigint       null,
    constraint UK_name
        unique (name),
    constraint FK_company_id
        foreign key (company_id) references companies (id),
    constraint FK_unit_parent_id
        foreign key (parent_id) references structural_units (id)
);
