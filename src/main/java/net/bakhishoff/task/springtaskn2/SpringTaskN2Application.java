package net.bakhishoff.task.springtaskn2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTaskN2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringTaskN2Application.class, args);
    }

}
