package net.bakhishoff.task.springtaskn2.repository;

/**
 * @author Ulphat
 */
public interface NameChecker {
    boolean existsByName(String name);
}
