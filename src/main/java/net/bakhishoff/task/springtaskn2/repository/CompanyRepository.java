package net.bakhishoff.task.springtaskn2.repository;

import net.bakhishoff.task.springtaskn2.model.entity.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.List;

/**
 * @author Ulphat
 */
public interface CompanyRepository extends JpaRepositoryImplementation<Company, Long>, NameChecker {

    List<Company> findAllByParentIsNull();
}
