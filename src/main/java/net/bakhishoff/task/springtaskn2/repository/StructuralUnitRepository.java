package net.bakhishoff.task.springtaskn2.repository;

import net.bakhishoff.task.springtaskn2.model.entity.StructuralUnit;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

/**
 * @author Ulphat
 */
public interface StructuralUnitRepository extends JpaRepositoryImplementation<StructuralUnit, Long>, NameChecker {

    boolean existsByNameAndIdNot(String name, long id);
}
