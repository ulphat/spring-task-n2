package net.bakhishoff.task.springtaskn2.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import net.bakhishoff.task.springtaskn2.exception.NoDataFoundException;
import net.bakhishoff.task.springtaskn2.model.dto.CompanyDto;
import net.bakhishoff.task.springtaskn2.model.mapper.CompanyMapper;
import net.bakhishoff.task.springtaskn2.model.request.CompanyRequest;
import net.bakhishoff.task.springtaskn2.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Ulphat
 */
@Service
@Transactional
@RequiredArgsConstructor
public class CompanyService {

    private final CompanyRepository repository;

    private final CompanyMapper mapper;

    public CompanyDto create(CompanyRequest request) {
        return Optional.ofNullable(request)
                       .map(mapper::toEntity)
                       .map(repository::save)
                       .map(mapper::toDto)
                       .orElseThrow();
    }

    /**
     * This method will return all companies and with their relations. This causes duplications in response.
     * So 1 company can appear in different tree
     * Alternatively it is possible return only root companies with their relations.
     * In this situation response will contain all companies without any duplication, but to access any non-root company,
     * it will be required moving over hierarchy
     *
     * @return lis of companies
     */
    public List<CompanyDto> list() {
        return repository.findAll()
                         .stream()
                         .map(mapper::toDto)
                         .collect(Collectors.toList());
    }

    public void delete(long id) {
        repository.findById(id)
                  .ifPresentOrElse(company -> {
                      if (company.getSubCompanies().isEmpty() && company.getUnits().isEmpty()) {
                          repository.deleteById(id);
                      } else {
                          throw new IllegalArgumentException("Cannot be deleted. It has relations");
                      }
                  }, () -> {
                      throw new NoDataFoundException();
                  });
    }
}
