package net.bakhishoff.task.springtaskn2.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import net.bakhishoff.task.springtaskn2.exception.NoDataFoundException;
import net.bakhishoff.task.springtaskn2.model.dto.CompanyWithSubsDto;
import net.bakhishoff.task.springtaskn2.model.dto.StructuralUnitDto;
import net.bakhishoff.task.springtaskn2.model.entity.StructuralUnit;
import net.bakhishoff.task.springtaskn2.model.mapper.StructuralUnitMapper;
import net.bakhishoff.task.springtaskn2.model.request.StructuralUnitRequest;
import net.bakhishoff.task.springtaskn2.repository.CompanyRepository;
import net.bakhishoff.task.springtaskn2.repository.StructuralUnitRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Ulphat
 */
@Service
@Transactional
@RequiredArgsConstructor
public class StructuralUnitService {

    private final StructuralUnitRepository repository;
    private final CompanyRepository companyRepository;
    private final StructuralUnitMapper mapper;

    public StructuralUnitDto create(StructuralUnitRequest request) {
        return Optional.of(request)
                       .map(mapper::toEntity)
                       .map(repository::save)
                       .map(mapper::toDto)
                       .orElseThrow();
    }

    public StructuralUnitDto update(long id, StructuralUnitRequest request) {
        return repository.findById(id)
                         .map(unit -> check(id, request, unit))
                         .map(unit -> mapper.toEntity(unit, request))
                         .map(repository::save)
                         .map(mapper::toDto)
                         .orElseThrow(NoDataFoundException::new);
    }

    public List<CompanyWithSubsDto> list() {
        return companyRepository.findAllByParentIsNull()
                         .stream()
                         .map(mapper::toDto)
                         .toList();
    }

    public void deleteById(long id) {
        repository.findById(id)
                  .ifPresentOrElse(unit -> {
                      if (unit.getSubUnits().isEmpty()) {
                          repository.deleteById(id);
                      } else {
                          throw new IllegalArgumentException("Cannot be deleted. It has relations");
                      }
                  }, () -> {
                      throw new NoDataFoundException();
                  });
    }

    private StructuralUnit check(long id, StructuralUnitRequest request, StructuralUnit unit) {
        if (unit.getCompany() == null && request.getCompanyId() != null) {
            throw new IllegalArgumentException("Cannot set company to the unit if it has not any previously");
        }
        if (unit.getParent() == null && request.getStructuralUnitId() != null) {
            throw new IllegalArgumentException("Cannot set parent to the unit if it has not any previously");
        }
        Optional.of(request)
                .map(StructuralUnitRequest::getStructuralUnitId)
                .flatMap(repository::findById)
                .ifPresent(parent -> {
                    while (parent != null) {
                        if (id == parent.getId()) {
                            throw new IllegalArgumentException(
                                    "Current unit is in the parent chain of the provided parent");
                        }
                        parent = parent.getParent();
                    }
                });
        if (repository.existsByNameAndIdNot(request.getName(), id))
            throw new IllegalArgumentException("Name already exists");
        return unit;
    }
}
