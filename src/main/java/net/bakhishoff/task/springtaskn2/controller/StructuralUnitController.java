package net.bakhishoff.task.springtaskn2.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import net.bakhishoff.task.springtaskn2.model.dto.CompanyWithSubsDto;
import net.bakhishoff.task.springtaskn2.model.dto.StructuralUnitDto;
import net.bakhishoff.task.springtaskn2.model.request.StructuralUnitRequest;
import net.bakhishoff.task.springtaskn2.model.request.validator.CreateUnit;
import net.bakhishoff.task.springtaskn2.model.request.validator.UpdateUnit;
import net.bakhishoff.task.springtaskn2.service.StructuralUnitService;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Ulphat
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/units")
public class StructuralUnitController {

    private final StructuralUnitService service;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public StructuralUnitDto create(@Validated(CreateUnit.class) @Valid @RequestBody StructuralUnitRequest request) {
        return service.create(request);
    }

    @PutMapping("/{id}")
    public StructuralUnitDto update(@PathVariable long id, @Validated(UpdateUnit.class) @Valid @RequestBody StructuralUnitRequest request) {
        return service.update(id, request);
    }

    @GetMapping
    public List<CompanyWithSubsDto> list() {
        return service.list();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long id) {
        service.deleteById(id);
    }
}
