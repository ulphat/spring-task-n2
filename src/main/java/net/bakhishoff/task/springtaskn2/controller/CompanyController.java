package net.bakhishoff.task.springtaskn2.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import net.bakhishoff.task.springtaskn2.model.dto.CompanyDto;
import net.bakhishoff.task.springtaskn2.model.request.CompanyRequest;
import net.bakhishoff.task.springtaskn2.service.CompanyService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Ulphat
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/companies")
public class CompanyController {

    private final CompanyService service;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyDto create(@Valid @RequestBody CompanyRequest request) {
        return service.create(request);
    }

    @GetMapping
    public List<CompanyDto> list(){
        return service.list();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long id){
        service.delete(id);
    }
}
