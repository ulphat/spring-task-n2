package net.bakhishoff.task.springtaskn2.exception.handler;

import net.bakhishoff.task.springtaskn2.exception.ExceptionBody;
import net.bakhishoff.task.springtaskn2.exception.IncorrectField;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Ulphat
 */
@ControllerAdvice
public class DefaultHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        return Optional.of(ex)
                       .map(BindException::getBindingResult)
                       .map(this::toExceptionBody)
                       .map(exceptionBody -> processErrorMessage(exceptionBody, HttpStatus.UNPROCESSABLE_ENTITY))
                       .orElse(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());//should never happen
    }

    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(IllegalArgumentException ex) {
        return this.processErrorMessage(new ExceptionBody(ex.getMessage()), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    private ResponseEntity<Object> processErrorMessage(ExceptionBody body, HttpStatus status) {
        return ResponseEntity.status(status).body(body);
    }

    private ExceptionBody toExceptionBody(BindingResult result) {
        return new ExceptionBody(getGlobalErrors(result), getFieldErrors(result));
    }

    private String getGlobalErrors(BindingResult result) {
        return result.getGlobalErrors()
                     .stream()
                     .map(DefaultMessageSourceResolvable::getDefaultMessage)
                     .collect(Collectors.joining(";"));
    }

    private List<IncorrectField> getFieldErrors(BindingResult result) {
        return result.getFieldErrors()
                     .stream()
                     .map((error) -> new IncorrectField(error.getField(), error.getDefaultMessage()))
                     .sorted(Comparator.comparing(IncorrectField::getPath))
                     .toList();
    }
}
