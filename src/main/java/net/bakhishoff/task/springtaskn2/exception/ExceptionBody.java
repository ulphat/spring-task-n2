package net.bakhishoff.task.springtaskn2.exception;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * @author Ulphat
 */
@Getter
@Setter
public class ExceptionBody {
    private String message;
    private List<IncorrectField> incorrectFields;
    private Date timestamp = new Date();

    public ExceptionBody(String message) {
        this.message = message;
    }

    public ExceptionBody(List<IncorrectField> incorrectFields) {
        this.incorrectFields = incorrectFields;
    }

    public ExceptionBody(String message, List<IncorrectField> incorrectFields) {
        this.message = message;
        this.incorrectFields = incorrectFields;
    }

}
