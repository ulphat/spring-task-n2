package net.bakhishoff.task.springtaskn2.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Ulphat
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IncorrectField {
    private String path;
    private String message;
}
