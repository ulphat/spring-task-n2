package net.bakhishoff.task.springtaskn2.model.request;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import net.bakhishoff.task.springtaskn2.model.request.validator.Exists;
import net.bakhishoff.task.springtaskn2.model.request.validator.Unique;
import net.bakhishoff.task.springtaskn2.repository.CompanyRepository;

/**
 * @author Ulphat
 */
@Getter
@Setter
public class CompanyRequest {

    @NotBlank
    @Unique(repository = CompanyRepository.class)
    private String name;

    @Exists(repository = CompanyRepository.class)
    private Long parentCompanyId;
}
