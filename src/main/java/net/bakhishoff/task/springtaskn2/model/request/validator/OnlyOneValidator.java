package net.bakhishoff.task.springtaskn2.model.request.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import net.bakhishoff.task.springtaskn2.model.request.StructuralUnitRequest;

/**
 * @author Ulphat
 */
public class OnlyOneValidator implements ConstraintValidator<OnlyOne, StructuralUnitRequest> {
    @Override
    public boolean isValid(StructuralUnitRequest value, ConstraintValidatorContext context) {
        return value == null || (value.getStructuralUnitId() == null ^ value.getCompanyId() == null);
    }
}
