package net.bakhishoff.task.springtaskn2.model.request.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Ulphat
 */
public class ExistsValidator implements ConstraintValidator<Exists, Long> {

    @Autowired
    private ApplicationContext context;

    private JpaRepository<?, Long> repository;

    public void initialize(Exists constraint) {
        this.repository = this.context.getBean(constraint.repository());
    }

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return value == null || repository.existsById(value);
    }
}
