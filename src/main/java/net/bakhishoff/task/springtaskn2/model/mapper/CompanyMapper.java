package net.bakhishoff.task.springtaskn2.model.mapper;

import net.bakhishoff.task.springtaskn2.model.dto.CompanyDto;
import net.bakhishoff.task.springtaskn2.model.entity.Company;
import net.bakhishoff.task.springtaskn2.model.request.CompanyRequest;
import net.bakhishoff.task.springtaskn2.repository.CompanyRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Ulphat
 */
@Mapper
public abstract class CompanyMapper {

    @Autowired
    protected CompanyRepository repository;

    @Mapping(target = "parent", source = "parentCompanyId")
    public abstract Company toEntity(CompanyRequest request);

    @Named("all")
    @Mapping(target = "parent", source = "parent", qualifiedByName = "ignoreSubs")
    public abstract CompanyDto toDto(Company company);

    @Named("ignoreSubs")
    @Mapping(target = "subCompanies", ignore = true)
    @Mapping(target = "parent", expression = "java(toDtoIgnoreSubs(company.getParent()))")
    public abstract CompanyDto toDtoIgnoreSubs(Company company);

    @Named("ignoreParent")
    @Mapping(target = "parent", ignore = true)
    public abstract CompanyDto toDtoIgnoreParent(Company company);

    public List<CompanyDto> toDto(List<Company> list) {
        if (list == null) return null;
        return list.stream()
                   .map(this::toDtoIgnoreParent)
                   .collect(Collectors.toList());
    }

    protected Company idToEntity(Long id) {
        return Optional.ofNullable(id)
                       .flatMap(repository::findById)
                       .orElse(null);
    }

}
