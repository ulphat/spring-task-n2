package net.bakhishoff.task.springtaskn2.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Ulphat
 */
@Getter
@Setter
public class CompanyWithSubsDto {

    private long id;

    private String name;

    private List<CompanyWithSubsDto> subCompanies;
    private List<StructuralUnitDto> units;
}
