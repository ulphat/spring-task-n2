package net.bakhishoff.task.springtaskn2.model.request;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;
import net.bakhishoff.task.springtaskn2.model.request.validator.*;
import net.bakhishoff.task.springtaskn2.repository.CompanyRepository;
import net.bakhishoff.task.springtaskn2.repository.StructuralUnitRepository;

/**
 * @author Ulphat
 */
@Getter
@Setter
@OnlyOne(groups = {CreateUnit.class, UpdateUnit.class})
public class StructuralUnitRequest {

    @NotEmpty(groups = {CreateUnit.class, UpdateUnit.class})
    @Unique(repository = StructuralUnitRepository.class, groups = CreateUnit.class)
    private String name;

    @Exists(repository = CompanyRepository.class, groups = {CreateUnit.class, UpdateUnit.class})
    private Long companyId;

    @Exists(repository = StructuralUnitRepository.class, groups = {CreateUnit.class, UpdateUnit.class})
    private Long structuralUnitId;
}
