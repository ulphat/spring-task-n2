package net.bakhishoff.task.springtaskn2.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Ulphat
 */
@Getter
@Setter
public class StructuralUnitDto {
    private long id;

    private String name;

    private List<StructuralUnitDto> subUnits;
}
