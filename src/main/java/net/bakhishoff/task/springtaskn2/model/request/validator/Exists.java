package net.bakhishoff.task.springtaskn2.model.request.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import net.bakhishoff.task.springtaskn2.repository.NameChecker;
import org.springframework.data.jpa.repository.JpaRepository;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Ulphat
 */
@Documented
@Constraint(validatedBy = {ExistsValidator.class})
@Target(FIELD)
@Retention(RUNTIME)
public @interface Exists {

    String message() default "{net.bakhishoff.task.springtaskn2.model.request.validator.Exists.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    Class<? extends JpaRepository<?, Long>> repository();
}
