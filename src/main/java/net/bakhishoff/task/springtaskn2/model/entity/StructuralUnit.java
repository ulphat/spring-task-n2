package net.bakhishoff.task.springtaskn2.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ulphat
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "structural_units")
public class StructuralUnit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String name;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private StructuralUnit parent;

    @OneToMany(mappedBy = "parent")
    private List<StructuralUnit> subUnits = new ArrayList<>();

    public StructuralUnit(long id) {
        this.id = id;
    }

    public StructuralUnit(long id, StructuralUnit parent) {
        this.id = id;
        this.parent = parent;
    }

    public StructuralUnit(long id, String name, StructuralUnit parent) {
        this.id = id;
        this.name = name;
        this.parent = parent;
    }
}
