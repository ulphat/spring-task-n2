package net.bakhishoff.task.springtaskn2.model.request.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Ulphat
 */
@Documented
@Constraint(validatedBy = {OnlyOneValidator.class})
@Target(TYPE)
@Retention(RUNTIME)
public @interface OnlyOne {
    String message() default "{net.bakhishoff.task.springtaskn2.annotation.OnlyOne.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
