package net.bakhishoff.task.springtaskn2.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Ulphat
 */
@Getter
@Setter
public class CompanyDto {

    private long id;

    private String name;

    private CompanyDto parent;

    private List<CompanyDto> subCompanies;
}
