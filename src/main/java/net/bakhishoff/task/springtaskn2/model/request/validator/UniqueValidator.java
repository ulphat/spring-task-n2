package net.bakhishoff.task.springtaskn2.model.request.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import net.bakhishoff.task.springtaskn2.repository.NameChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * @author Ulphat
 */
public class UniqueValidator implements ConstraintValidator<Unique, String> {

    @Autowired
    private ApplicationContext context;

    private NameChecker repository;

    public void initialize(Unique constraint) {
        this.repository = this.context.getBean(constraint.repository());
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || !repository.existsByName(value);
    }
}
