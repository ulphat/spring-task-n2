package net.bakhishoff.task.springtaskn2.model.mapper;

import net.bakhishoff.task.springtaskn2.model.dto.CompanyWithSubsDto;
import net.bakhishoff.task.springtaskn2.model.dto.StructuralUnitDto;
import net.bakhishoff.task.springtaskn2.model.entity.Company;
import net.bakhishoff.task.springtaskn2.model.entity.StructuralUnit;
import net.bakhishoff.task.springtaskn2.model.request.StructuralUnitRequest;
import net.bakhishoff.task.springtaskn2.repository.StructuralUnitRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * @author Ulphat
 */
@Mapper(uses = CompanyMapper.class)
public abstract class StructuralUnitMapper {

    @Autowired
    protected StructuralUnitRepository repository;

    @Mapping(target = "company", source = "companyId")
    @Mapping(target = "parent", source = "structuralUnitId")
    public abstract StructuralUnit toEntity(StructuralUnitRequest request);

    @Mapping(target = "company", source = "companyId")
    @Mapping(target = "parent", source = "structuralUnitId")
    public abstract StructuralUnit toEntity(@MappingTarget StructuralUnit unit, StructuralUnitRequest request);

    public abstract StructuralUnitDto toDto(StructuralUnit structuralUnit);

    public abstract CompanyWithSubsDto toDto(Company company);

    @Named("ignoreSubs")
    @Mapping(target = "subUnits", ignore = true)
    public abstract StructuralUnitDto toDtoIgnoreSubs(StructuralUnit structuralUnit);


    protected StructuralUnit idToEntity(Long id) {
        return Optional.ofNullable(id)
                       .flatMap(repository::findById)
                       .orElse(null);
    }
}
