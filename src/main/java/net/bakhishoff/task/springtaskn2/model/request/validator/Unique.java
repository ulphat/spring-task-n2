package net.bakhishoff.task.springtaskn2.model.request.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import net.bakhishoff.task.springtaskn2.repository.NameChecker;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Ulphat
 */
@Documented
@Constraint(validatedBy = {UniqueValidator.class})
@Target(FIELD)
@Retention(RUNTIME)
public @interface Unique {
    String message() default "{net.bakhishoff.task.springtaskn2.annotation.Unique.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    Class<? extends NameChecker> repository();
}
