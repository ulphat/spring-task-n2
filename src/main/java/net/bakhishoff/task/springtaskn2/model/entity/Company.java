package net.bakhishoff.task.springtaskn2.model.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ulphat
 */
@Getter
@Setter
@Entity
@Table(name = "companies")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String name;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Company parent;

    @OneToMany(mappedBy = "parent")
    private List<Company> subCompanies = new ArrayList<>();

    @OneToMany(mappedBy = "company")
    private List<StructuralUnit> units = new ArrayList<>();

}
